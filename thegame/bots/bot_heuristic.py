import itertools

from thegame.bots.bot_base import BotBase, CardAction


class BotHeuristic(BotBase):
    '''Bot using a heuristic to select best turn from all possibilities'''

    def __init__(self, *, max_turn_length=2):
        self.max_turn_length = max_turn_length

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        best_actions = []
        best_value = float("-inf")
        # check different turn lengths
        for turn_length in range(min(target_length, self.max_turn_length), self.max_turn_length + 1):
            # evaluate all possibilities of a given turn length
            for actions in self.generate_possible_actions(stacks, cards, turn_length):
                value = self.heuristic(stacks, cards, actions)
                if value > best_value:
                    best_value = value
                    best_actions = actions.copy()
        return best_actions

    def heuristic(self, stacks, cards, actions):
        '''heuristic to evaluate actions'''
        stack_worth = sum(stack.lim_high + stack.lim_low - stack.value
                          if stack.incrementing else stack.value for stack in stacks)
        return stack_worth + len(actions)


class BotHeuristicFast(BotHeuristic):
    '''
    Bot using a heuristic to select best turn.
    Does not check all possibilities but selects best stacks when placing cards
    '''

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        best_actions = []
        best_value = float("-inf")
        # check different turn lengths
        for turn_length in range(min(target_length, self.max_turn_length), self.max_turn_length + 1):
            # check all possibilities for a given turn length
            card_sequences = itertools.permutations(cards, turn_length)
            for card_sequence in card_sequences:
                actions = []
                for card in card_sequence:
                    stack_id = self._find_best_stack(stacks, card)
                    if stack_id is not None and stacks[stack_id].is_placeable(card):
                        stacks[stack_id].place_card(card)
                        actions.append(CardAction(card=card, stack_id=stack_id))
                    else:
                        break
                if len(actions) == turn_length:
                    value = self.heuristic(stacks, cards, actions)
                    if value > best_value:
                        best_value = value
                        best_actions = actions.copy()
                for action in reversed(actions):
                    stacks[action.stack_id].pop_card()
        return best_actions

    def _find_best_stack(self, stacks, card):
        best_id = None
        best_diff = float("inf")
        for stack_id, stack in enumerate(stacks):
            if stack.is_placeable(card):
                # calculate without abs() to favor jumps
                stack_diff = card - stack.value if stack.incrementing else stack.value - card
                if stack_diff < best_diff:
                    best_diff = stack_diff
                    best_id = stack_id
        return best_id


class BotHeuristicCardCounter(BotHeuristic):
    '''Heuristic Bot that keeps track of remaining cards in game'''

    def __init__(self, *, stack_lut=None, **kwargs):
        super().__init__(**kwargs)
        self._stack_lut = stack_lut if stack_lut is not None else [-999999, 1, 50, 60, 65]  # determined by try-n-error
        self._remaining_cards = set()
        self._all_cards = None

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        # determine remaining cards in game
        placed_cards = set(c for s in stacks for c in s._history[1:])
        try:
            self._remaining_cards = self._all_cards.difference(placed_cards)
        except AttributeError:
            self._all_cards = set(range(stacks[0].lim_low+1, stacks[0].lim_high))
            self._remaining_cards = self._all_cards.difference(placed_cards)
        # evaluate all possibilities
        return super().compute_turn(stacks, cards, target_length, **kwargs)

    def heuristic(self, stacks, cards, actions):
        for action in actions:
            self._remaining_cards.remove(action.card)
        result = 0
        for c in self._remaining_cards:
            card_worth = self._stack_lut[sum(int(stack.is_placeable(c)) for stack in stacks)]
            # give hand cards more importance
            if c in cards:
                card_worth *= 1.0 + (len(self._remaining_cards)-(len(cards)-len(actions)))/len(self._all_cards)
            result += card_worth
        for action in actions:
            self._remaining_cards.add(action.card)
        return result
