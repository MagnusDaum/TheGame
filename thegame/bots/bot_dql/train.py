import argparse
import copy
import time
import random
import os

import numpy as np
from matplotlib import pyplot as plt

from thegame.game_handler import GameHandler, GameHandlerOnFire, GameStatus
from thegame.bots.bot_dql import BotDQL


def _train_n_games(bot, num_games):
    game = GameHandler(num_players=1)
    for _ in range(num_games):
        game.reset(new_deck=True)
        bot.reset()
        while game.get_status() == GameStatus.ONGOING:
            action = bot.compute_turn(game.stacks, game.cards[game.player_id], 1)[0]
            if not game.do_action(action):
                raise RuntimeError  # sanity check
            bot.fit_post_turn(game.get_status(), game.stacks, game.cards[game.player_id])
            game.end_turn()
        yield game.get_total_cards_placed()


def train_endlessly(bot, alpha_start, epsilon_start, output_dir):
    bot.alpha = alpha_start
    alpha_decay = 0.98
    alpha_min = 0.1

    bot.epsilon = epsilon_start
    epsilon_decay = 0.95
    epsilon_min = 0.001  # to reach the endgame, low exploration is needed

    bot.gamma = 0.9

    save_path = os.path.join(output_dir, 'bot_dql.h5')
    save_tmp_path = os.path.join(output_dir, 'bot_dql_game{game_id:0>7}.h5')
    log_path = os.path.join(output_dir, 'bot_dql_training.txt')
    with open(log_path, 'a') as out_file:
        out_file.write('#'*80+'\n')
        out_file.write(f'starting training on {time.strftime("%Y-%m-%d %H:%M", time.localtime(time.time()))}\n')

    game_id = 0
    test_game_cards_placed = 0
    while True:
        if game_id % 10000 == 0:
            print('---\ntesting bot performance: playing a game without random actions')
            eps = bot.epsilon
            bot.epsilon = 0.0
            rand_state = random.getstate()
            random.seed(1936792095)  # this seed was solveable by BotHeuristic
            game = GameHandler(num_players=1)
            game.play([bot])
            random.setstate(rand_state)
            bot.epsilon = eps
            print(f'game state: {game.get_status().name} cards_placed: {game.get_total_cards_placed()}')
            bot.save(save_tmp_path.format(game_id=game_id))
            with open(log_path, 'a') as out_file:
                out_file.write(f'game: {game_id:>7} alpha: {bot.alpha:.2f} epsilon: {bot.epsilon:.2f}'
                               f' cards placed: {game.get_total_cards_placed():>3}\n')
            if game.get_total_cards_placed() < test_game_cards_placed - 10:
                bot.epsilon = bot.epsilon ** 0.5  # more exploration needed
            test_game_cards_placed = game.get_total_cards_placed()
            print('---')

        print(f'game: {game_id} alpha: {bot.alpha:.2f} epsilon: {bot.epsilon:.2f}')
        step = 1000
        results = _train_n_games(bot, step)
        cards_placed = sum(results)
        print(f'mean cards placed: {cards_placed / step}')
        game_id += step

        bot.alpha = max(alpha_min, bot.alpha * alpha_decay)
        bot.epsilon = max(epsilon_min, bot.epsilon * epsilon_decay)


def _plotax_bot_prediction(ax, prediction, rewards):
    ax.plot(range(len(prediction)), prediction, label='prediction')
    ax.plot(range(len(rewards)), rewards, label='reward')
    ax.set_xlabel('action')
    ax.set_xticks([i for i in range(len(prediction))], minor=True)
    ax.set_xticks([i for i in range(0, len(prediction), len(prediction)//4)])
    ax.xaxis.grid(True, which='major')
    ax.legend()


def _calculate_rewards(bot, stacks, cards):
    rewards = []
    for action in bot._get_all_actions(cards):
        if action.card > 0 and stacks[action.stack_id].is_placeable(action.card):
            stacks[action.stack_id].place_card(action.card)
            cards.remove(action.card)
            rewards.append(bot._calculate_reward(GameStatus.ONGOING, stacks, cards))
            cards = sorted(cards + [action.card])
            stacks[action.stack_id].pop_card()
        else:
            rewards.append(np.nan)
    return rewards


def check_first_move(bot, plot=False):
    cards_to_test = [list(range(2, 10)), list(range(92, 100)), sorted(random.sample(range(2, 100), k=8))]
    if plot:
        fig = plt.figure(figsize=(16, 9))
    game = GameHandler([bot])
    for test_idx, cards in enumerate(cards_to_test):
        state = bot.generate_state(game.stacks, cards)
        print('evaluating state:', state)
        prediction = bot._model.predict(state).flatten()
        print('prediction minmax ', prediction.argmin(), np.min(prediction), prediction.argmax(), np.max(prediction))
        rewards = _calculate_rewards(bot, game.stacks, cards)
        rmse = np.sqrt(np.nanmean((prediction-rewards)**2))
        print('prediction-reward rmse:', rmse)
        if plot:
            ax = fig.add_subplot(1, len(cards_to_test), 1+test_idx, title=str(cards))
            _plotax_bot_prediction(ax, prediction, rewards)
    print('---')


def train_first_move(bot):
    alpha_decay = 0.8
    bot.alpha = 0.5
    bot.gamma = 0.0  # we only train first move so future rewards don't matter
    bot.epsilon = 1.0  # explore

    game = GameHandler([bot])
    for i in range(10):
        print('---', i, '---')
        for _ in range(1000):
            game.cards[0] = sorted(random.sample(range(2, 100), k=8))
            actions = bot.compute_turn(game.stacks, game.cards[0], 2)
            for action in actions:
                if not game.stacks[action.stack_id].is_placeable(action.card):
                    raise RuntimeError
                game.stacks[action.stack_id].place_card(action.card)
                game.cards[0].remove(action.card)
                bot.fit_post_turn(game.get_status(), game.stacks, game.cards[0])
                game.stacks[action.stack_id].pop_card()
        check_first_move(bot)
        bot.alpha *= alpha_decay

    return bot


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--alpha', help='alpha at the start of training', type=float, default=0.1)
    parser.add_argument('-e', '--epsilon', help='epsilon at the start of training', type=float, default=1.0)
    parser.add_argument('-l', '--load_path', help='path for loading stored bot', type=str)
    parser.add_argument('-o', '--output_dir', help='path for storing training output', type=str, default=os.getcwd())
    parser.add_argument('-s', '--seed', help='random seed', type=int, default=42)
    args, _ = parser.parse_known_args()

    random.seed(args.seed)
    np.random.seed(args.seed)

    bot_dql = BotDQL(args.load_path)
    print(bot_dql._model.summary())

    start_time = time.time()

    try:
        # check_first_move(bot_dql, plot=True)
        # train_first_move(bot_dql)
        # bot_dql.save('bot_dql_first_move.h5')

        train_endlessly(bot_dql, alpha_start=args.alpha, epsilon_start=args.epsilon, output_dir=args.output_dir)
    except KeyboardInterrupt:
        bot_dql.save(os.path.join(args.output_dir, 'bot_dql.h5'))

    end_time = time.time()
    print(f'took {round(end_time-start_time, 3)} seconds')

    check_first_move(bot_dql, plot=True)
    plt.show()
