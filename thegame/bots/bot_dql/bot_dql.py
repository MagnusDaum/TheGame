import os
import random

import numpy as np
from tensorflow import keras

from thegame.game_handler import GameStatus
from thegame.bots.bot_base import BotBase, CardAction


class BotDQL(BotBase):
    '''Bot utilizing Deep Q-Learning to find best turn'''

    NUM_PLAYER_CARDS = 8
    NUM_STACKS = 4
    STATE_SIZE = NUM_STACKS + NUM_PLAYER_CARDS  # + 98

    MAX_REWARD = 100000.0

    def __init__(self, load_path=None, *, alpha=0.0, epsilon=0.0, gamma=0.8, batch_size=16):
        '''constructor
        Parameters
        ----------
        load_path: where to load model from. if None create new one
        alpha: learning rate
        gamma: discount rate
        epsilon: epsilon greedy for exploration=1.0 vs exploitation=0.0
        '''
        super().__init__()
        self.epsilon = epsilon
        self.alpha = alpha
        self.gamma = gamma

        self.batch_size = batch_size

        if load_path is None:
            self._model = self._generate_model()
        else:
            self.load(load_path)

        self.reset()

    def reset(self):
        super().reset()
        # store inputs/outputs for training model
        self._action_id = None
        self._sample_id = 0
        self._in_states = np.empty((self.batch_size, self.STATE_SIZE))
        self._out_states = np.empty((self.batch_size, self.NUM_STACKS * self.NUM_PLAYER_CARDS))

    def load(self, file_path):
        if not os.path.isfile(file_path):
            raise FileNotFoundError(f'Could not find {file_path}')
        try:
            self._model = keras.models.load_model(file_path)
        except ValueError:  # file does not contain full model -> load weights
            self._model = self._generate_model()
            self._model.load_weights(file_path)

    def save(self, file_path, weights_only=False):
        if weights_only:
            self._model.save_weights(file_path)
        else:
            self._model.save(file_path)

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        best_turn = []

        all_actions = self._get_all_actions(cards)
        valid_actions = np.array([stacks[a.stack_id].is_placeable(a.card) and a.card > 0 for a in all_actions])
        if np.any(valid_actions):
            self._in_states[self._sample_id] = self.generate_state(stacks, cards)

            if random.random() > self.epsilon:
                # exploitation: best predicted action
                prediction = self._model.predict(self._in_states[self._sample_id].reshape(1, -1)).flatten()
                self._action_id = np.where(valid_actions, prediction, np.nanmin(prediction)-1).argmax()
            else:
                # exploration: random action
                self._action_id = np.random.choice(np.arange(len(all_actions))[valid_actions])
            best_turn += [all_actions[self._action_id]]

        return best_turn

    def fit_post_turn(self, game_status, stacks, cards):
        '''update model post-turn'''
        # get reward for current turn
        reward = self._calculate_reward(game_status, stacks, cards)
        # estimate reward for next turn
        if game_status == GameStatus.ONGOING:
            next_state = self.generate_state(stacks, cards)
            next_prediction = self._model.predict(next_state).flatten()
            all_actions = self._get_all_actions(cards)
            valid_actions = np.array([stacks[a.stack_id].is_placeable(a.card) and a.card > 0 for a in all_actions])
            future_reward = np.where(valid_actions, next_prediction, np.nanmin(next_prediction)-1).max()
        else:
            future_reward = reward
        # combine current and future rewards
        target_q = reward + self.gamma * future_reward
        # compute model target according to reward
        self._out_states[self._sample_id] = self._model.predict(self._in_states[self._sample_id].reshape(1, -1))
        self._out_states[self._sample_id][self._action_id] = self.alpha * target_q + \
            (1.0-self.alpha) * self._out_states[self._sample_id][self._action_id]
        self._sample_id += 1
        # fit model if batch is full
        if self._sample_id >= self.batch_size:
            self._sample_id = 0
            self._model.fit(self._in_states, self._out_states, batch_size=self.batch_size, epochs=1, verbose=0)

    ###########################################################################

    @classmethod
    def _generate_model(cls):
        inputs = keras.layers.Input(shape=(12,))
        dense0 = keras.layers.Dense(256, activation='relu')(inputs)
        dense1 = keras.layers.Dense(128, activation='relu')(dense0)
        dense2 = keras.layers.Dense(64, activation='relu')(dense1)
        outputs = keras.layers.Dense(32, activation='linear')(dense2)
        model = keras.Model(inputs=inputs, outputs=outputs)
        model.compile(loss='mse', optimizer=keras.optimizers.Adam())
        return model

    @classmethod
    def generate_state(cls, stacks, cards):
        # return numpy array([[ ]]) to fit keras input
        # np.array([ ]) would be interpreted as n samples of input size 1 instead of 1 sample of input size n
        # TODO normalize these values?
        return np.array([[stacks[st].value for st in range(cls.NUM_STACKS)] + cls._pad_cards(cards)])

    @classmethod
    def _pad_cards(cls, cards):
        # padded to always have same length
        return [0] * (cls.NUM_PLAYER_CARDS - len(cards)) + sorted(cards)

    @classmethod
    def _get_all_actions(cls, cards):
        actions = np.array([CardAction(card=c, stack_id=st) for st in range(cls.NUM_STACKS)
                            for c in cls._pad_cards(cards)])
        return actions

    @classmethod
    def _calculate_reward(cls, game_status, stacks, cards):
        stack_len = sum(len(stack) for stack in stacks)

        if game_status == GameStatus.WON:
            reward = cls.MAX_REWARD
        elif game_status == GameStatus.LOST:
            reward = -cls.MAX_REWARD + stack_len
        else:
            # TODO how to make a optimistic Q initialization?
            # until then make rewards negative
            stack_worth = sum(
                -stack.value if stack.incrementing else stack.value - stack.lim_low - stack.lim_high
                for stack in stacks)
            reward = stack_worth + stack_len
        return reward
