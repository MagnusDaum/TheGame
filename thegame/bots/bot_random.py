import random

from thegame.bots.bot_base import BotBase, CardAction


class BotRandom(BotBase):
    '''Bot randomly selecting actions to determine next turn'''

    def __init__(self, max_turn_length=None):
        self.max_length = max_turn_length if max_turn_length is not None else float("inf")

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        next_turn = []
        target_len = random.randint(min(target_length, len(cards)), min(self.max_length, len(cards)))
        cards_shuffled = random.sample(cards, k=len(cards))
        while len(next_turn) < target_len and cards_shuffled:
            card = cards_shuffled.pop()
            stacks_shuffled = random.sample(range(len(stacks)), k=len(stacks))
            while stacks_shuffled:
                stack_id = stacks_shuffled.pop()
                if stacks[stack_id].is_placeable(card):
                    stacks[stack_id].place_card(card)
                    next_turn += [CardAction(card=card, stack_id=stack_id)]
                    break
        return next_turn
