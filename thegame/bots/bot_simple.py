from thegame.bots.bot_base import BotBase


class BotSimple(BotBase):
    '''Bot selecting first possible actions'''

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        return next(self.generate_possible_actions(stacks, cards, target_length))
