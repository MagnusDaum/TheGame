import bisect
import copy
import random

import numpy as np

from thegame.bots.bot_base import BotBase, CardAction
from thegame.bots.bot_heuristic import BotHeuristic
try:
    from thegame.bots._bot_monte_carlo import BotMonteCarlo as _BotMonteCarloCpp
except ModuleNotFoundError as e:
    # C++ pybind11 lib not available. Do not raise exception, as python implementation is still useable
    pass

class BotMonteCarlo(BotBase):
    '''Bot using Monte Carlo tree search to find best turn. Wrapper of C++ Implementation'''

    def __init__(self, **kwargs):
        try:
            self._bot = _BotMonteCarloCpp(**kwargs)
        except NameError:
            raise RuntimeError(
                "BotMonteCarlo: C++ Implementation not available! You can still use BotMonteCarloPython as a fallback.")
        self._all_cards = None

    def seed(self, seed):
        self._bot.seed(seed)

    def compute_turn(self, stacks, cards, cards_per_player, player_id, target_length, **kwargs):
        # determine remaining cards in game
        placed_cards = set(c for s in stacks for c in s._history[1:])
        try:
            remaining_cards = self._all_cards.difference(placed_cards)
        except AttributeError:
            self._all_cards = set(range(stacks[0].lim_low+1, stacks[0].lim_high))
            remaining_cards = self._all_cards.difference(placed_cards)
        remaining_cards = remaining_cards.difference(set(cards))

        turn = self._bot.compute_turn(list(remaining_cards), [s.value for s in stacks],
                                      cards, cards_per_player, player_id, target_length)
        turn = [CardAction(stack_id=t[0], card=t[1]) for t in turn]
        return turn


class BotMonteCarloPython(BotBase):
    '''Bot using Monte Carlo tree search to find best turn'''

    def __init__(self, *, num_rollouts, rollout_width=None, rollout_depth=100, uct_c=20, activation_threshold=0):
        self.num_rollouts = num_rollouts  # number of rollouts per turn
        self.rollout_width = rollout_width  # only roll out N promising actions
        self.rollout_depth = rollout_depth  # only roll out until depth
        self.uct_c = uct_c  # Upper Confidence Bound for Trees constant

        self._all_cards = None
        # for faster execution use helper bot in early game
        self._helper_bot = BotHeuristic()
        self._activation_threshold = activation_threshold

    def compute_turn(self, stacks, cards, target_length, **kwargs):
        if sum(len(stack) for stack in stacks) < self._activation_threshold:
            return self._helper_bot.compute_turn(stacks, cards, target_length, **kwargs)

        if self.rollout_width is None:
            all_turns = list(BotBase.generate_possible_actions(stacks, cards, 1))
        else:
            all_turns = []
            turn_values = []
            for actions in BotBase.generate_possible_actions(stacks, cards, 1):
                all_turns.append(actions)
                turn_values.append(-sum(stack.lim_high + stack.lim_low - stack.value
                                        if stack.incrementing else stack.value for stack in stacks))
            if len(all_turns) > self.rollout_width:
                kth_idx = min(self.rollout_width, len(turn_values))
                turn_values_sorted = np.argpartition(turn_values, kth_idx)
                all_turns = np.array(all_turns)[turn_values_sorted][:kth_idx].tolist()

        if len(all_turns) == 1:
            return all_turns[0]

        # determine remaining cards in game
        placed_cards = set(c for s in stacks for c in s._history[1:])
        try:
            remaining_cards = self._all_cards.difference(placed_cards)
        except AttributeError:
            self._all_cards = set(range(stacks[0].lim_low+1, stacks[0].lim_high))
            remaining_cards = self._all_cards.difference(placed_cards)
        remaining_cards = remaining_cards.difference(set(cards))

        rewards = np.zeros(len(all_turns))
        visits = np.zeros(len(all_turns))

        num_target_cards = len(cards)
        for roll_id in range(self.num_rollouts):
            if roll_id < len(all_turns):
                turn_id = roll_id
            else:
                uct = self._compute_uct(visits, rewards)
                turn_id = np.argmax(uct)

            for action in all_turns[turn_id]:
                stacks[action.stack_id].place_card(action.card)
                cards.remove(action.card)

            deck = random.sample(remaining_cards, len(remaining_cards))

            rewards[turn_id] += self._rollout_game(deck, copy.deepcopy(stacks),
                                                   copy.deepcopy(cards), num_target_cards, self.rollout_depth)
            visits[turn_id] += 1

            bisect.insort(cards, action.card)
            stacks[action.stack_id].pop_card()

        best_turn = all_turns[np.nanargmax(rewards/visits)]
        return best_turn

    def _compute_uct(self, visits, rewards):
        return rewards/visits + self.uct_c * np.sqrt(np.log(np.sum(visits)) / visits)

    @staticmethod
    def _rollout_game(deck, stacks, cards, num_target_cards, depth):
        num_cards_to_draw = min(num_target_cards - len(cards), len(deck))
        for _ in range(num_cards_to_draw):
            cards.append(deck.pop())

        for turn_id in range(depth):
            if not cards:
                break
            best_turn = None
            best_value = float("-inf")
            for actions in BotBase.generate_possible_actions(stacks, cards, 1):
                value = sum(stack.lim_high + stack.lim_low - stack.value
                            if stack.incrementing else stack.value for stack in stacks)
                if value > best_value:
                    best_value = value
                    best_turn = actions.copy()
            if best_turn is None:
                turn_id -= 1
                break
            for action in best_turn:
                stacks[action.stack_id].place_card(action.card)
                cards.remove(action.card)
                if deck:
                    cards.append(deck.pop())  # cards become unsorted

        reward = sum(len(stack) for stack in stacks)
        if turn_id == depth - 1:  # game still ongoing
            reward += sum(stack.lim_high + stack.lim_low - stack.value
                          if stack.incrementing else stack.value for stack in stacks)
        return reward
