from abc import ABC, abstractmethod
import itertools

from thegame.core import CardAction


class BotBase(ABC):
    '''base class for all bots'''

    def reset(self):
        pass

    @abstractmethod
    def compute_turn(self, stacks, cards, target_length, cards_per_player, player_id):
        '''
        computes an iterable of CardActions.

        Parameters
        ----------
        stacks: list of CardStacks
        cards: list of int
            hand cards of current player
        target_length: int
            for convenience, minimum of how many actions are definitely possible and how many actions are needed
            to finish the current turn. function may return fewer or more actions
        cards_per_player: list of int
        player_id: int
        '''
        pass

    @staticmethod
    def generate_possible_actions(stacks, cards, target_length):
        '''
        generator yielding all possible actions of length target_length for given stacks and cards.
        Caution: For better performance, actions are returned before stacks are reset!'''
        card_sequences = itertools.permutations(cards, target_length)
        stack_sequences = [p for p in itertools.product(range(len(stacks)), repeat=target_length)]
        for card_sequence in card_sequences:
            for stack_sequence in stack_sequences:
                actions = []
                for card, stack_id in zip(card_sequence, stack_sequence):
                    if stacks[stack_id].is_placeable(card):
                        stacks[stack_id].place_card(card)
                        actions.append(CardAction(card=card, stack_id=stack_id))
                    else:
                        break
                if len(actions) == target_length:
                    yield actions
                for action in reversed(actions):
                    stacks[action.stack_id].pop_card()
