import random


class CardDeck:
    '''class from which cards can be drawn'''

    def __init__(self, lowest_card, highest_card, cards=None):
        self._low = lowest_card
        self._high = highest_card
        # keep a fixed (random) order of cards so draws can be reproduced
        if cards is not None:
            self._cards = cards.copy()
            self.reset(shuffle=False)
        else:
            self.reset(shuffle=True)

    def reset(self, shuffle=False):
        if shuffle:
            # reset cards so shuffling does not depend on previous state
            self._cards = list(range(self._low, self._high+1))
            random.shuffle(self._cards)
        self._end = len(self._cards)

    def __len__(self):
        return self._end

    def draw(self, num_cards):
        num_cards = min(num_cards, self._end)  # only draw if there are cards left
        cards = self._cards[self._end-num_cards:self._end]
        self._end -= len(cards)
        return cards

    @property
    def cards(self):
        return self._cards[:self._end]
