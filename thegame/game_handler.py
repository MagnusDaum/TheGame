import copy
from enum import Enum
import random

import numpy as np
import yaml

from thegame.core import CardAction
from thegame.cardstack import CardStack
from thegame.carddeck import CardDeck
from thegame.bots.bot_base import BotBase


class GameStatus(Enum):
    ONGOING = 0
    WON = 1
    LOST = 2


class GameHandler:

    CARDS_PER_PLAYER = [8, 7, 6, 6, 6]
    MAX_PLAYERS = len(CARDS_PER_PLAYER)
    CARDS_PER_TURN = 2
    CARDS_PER_TURN_EMPTY_DECK = 1

    def __init__(self, num_players=1, num_stacks=4, lower_limit=1, upper_limit=100, deck_cards=None):
        assert 0 < num_players <= self.MAX_PLAYERS
        self.cards = [[] for _ in range(num_players)]
        self.stacks = [CardStack(lower_limit, upper_limit, i < num_stacks // 2) for i in range(num_stacks)]
        self._deck = CardDeck(lower_limit+1, upper_limit-1, cards=deck_cards)

        self.reset()

    def reset(self, new_deck=False):
        self._deck.reset(shuffle=new_deck)
        self.cards = [sorted(self._deck.draw(self.CARDS_PER_PLAYER[len(self.cards) - 1])) for _ in self.cards]
        for stack in self.stacks:
            stack.reset()

        self.player_id = 0
        self.current_turn = []
        self.turn_history = []

    def save(self, file_path):
        '''store current game state to file'''
        with open(file_path, 'w') as out_file:
            game_dict = {"stacks": [s._history for s in self.stacks], "cards": self.cards,
                         "deck": self._deck._cards, "deck_end": self._deck._end}
            if self.player_id != 0:
                game_dict["player_id"] = self.player_id
            if self.current_turn:
                game_dict["current_turn"] = [[a.stack_id, a.card] for a in self.current_turn]
            if self.turn_history:
                game_dict["turn_history"] = [[p, [[a.stack_id, a.card] for a in t]] for p, t in self.turn_history]
            yaml.dump(game_dict, out_file, sort_keys=False, default_flow_style=False, width=80)

    def load_seed(self, seed):
        '''load game state via seed'''
        random.seed(seed)
        np.random.seed(seed)  # not used here, but maybe by bots
        self.reset(new_deck=True)

    def load_from_file(self, file_path):
        '''load current game state from file'''
        with open(file_path, 'r') as in_file:
            game_dict = yaml.safe_load(in_file)
        lower_limit = min([s[0] for s in game_dict["stacks"]])
        upper_limit = max([s[0] for s in game_dict["stacks"]])
        assert len(game_dict["deck"]) == upper_limit - lower_limit - 1 \
            and lower_limit < min(game_dict["deck"]) and max(game_dict["deck"]) < upper_limit
        self.cards = [sorted(c) for c in game_dict["cards"]]
        self.stacks = [CardStack(lower_limit, upper_limit, s[0] == lower_limit, s) for s in game_dict["stacks"]]
        self._deck = CardDeck(lower_limit+1, upper_limit-1, game_dict["deck"])
        self._deck._end = game_dict["deck_end"]

        self.player_id = game_dict.get("player_id", 0)
        self.current_turn = [CardAction(stack_id=a[0], card=a[1]) for a in game_dict.get("current_turn", [])]
        self.turn_history = \
            [(p, [CardAction(stack_id=a[0], card=a[1]) for a in t]) for p, t in game_dict.get("turn_history", [])]

    @property
    def num_players(self):
        return len(self.cards)

    def get_status(self):
        if 0 == self.get_total_cards_left():
            return GameStatus.WON

        if not self.can_end_turn() and not self._is_any_action_possible():
            return GameStatus.LOST

        return GameStatus.ONGOING

    def get_total_cards_left(self):
        return len(self._deck) + sum(len(c) for c in self.cards)

    def get_total_cards_placed(self):
        return sum((len(stack) for stack in self.stacks))

    def play(self, players):
        '''do turns until active player is not a bot or game is over'''
        assert len(players) == self.num_players
        while isinstance(players[self.player_id], BotBase) and self.get_status() == GameStatus.ONGOING:
            turn_length = self._get_num_cards_to_place()
            if turn_length > 0:
                # pass deepcopied objects so bots can not break game
                copy_stacks = copy.deepcopy(self.stacks)
                copy_cards = copy.deepcopy(self.cards[self.player_id])
                # check if turn of given length is possible
                try:
                    actions = next(BotBase.generate_possible_actions(copy_stacks, copy_cards, turn_length))
                    for action in reversed(actions):
                        copy_stacks[action.stack_id].pop_card()
                except StopIteration:
                    turn_length = 1
                # call bot
                actions = players[self.player_id].compute_turn(
                    stacks=copy_stacks, cards=copy_cards, target_length=turn_length,
                    cards_per_player=[len(c) for c in self.cards], player_id=self.player_id)
                for action in actions:
                    if not self.do_action(action):
                        raise RuntimeError('Invalid CardAction')
            self.end_turn()

    def play_turns(self, turns):
        '''apply turns to current game'''
        for player_id, actions in turns:
            if player_id != self.player_id:
                raise RuntimeError(f'Invalid Player: {player_id}')
            for action in actions:
                if not self.do_action(action):
                    raise RuntimeError('Invalid CardAction')
            if self.can_end_turn():
                self.end_turn()
            elif self.get_status() == GameStatus.ONGOING:
                raise RuntimeError('Invalid Turn')

    def do_action(self, action):
        if action.card in self.cards[self.player_id] and self.stacks[action.stack_id].is_placeable(action.card):
            self.stacks[action.stack_id].place_card(action.card)
            self.cards[self.player_id].remove(action.card)
            self.current_turn.append(action)
            return True
        return False

    def undo_action(self):
        if self.current_turn:
            action = self.current_turn.pop()
            card = self.stacks[action.stack_id].pop_card()
            assert card == action.card
            self.cards[self.player_id] = sorted(self.cards[self.player_id] + [action.card])
            return True
        return False

    def can_end_turn(self):
        return self._get_num_cards_to_place() <= 0

    def end_turn(self):
        '''end turn for current player. fill up cards and switch to next player'''
        if self.can_end_turn():
            self.cards[self.player_id] = sorted(self.cards[self.player_id] + self._deck.draw(len(self.current_turn)))
            self.turn_history.append((self.player_id, self.current_turn))
            self.current_turn = []
            self._switch_player()
            return True
        return False

    def _switch_player(self, step=1):
        original_player = self.player_id
        self.player_id = (self.player_id + step) % len(self.cards)
        # if next player has no cards, switch again
        while original_player != self.player_id and not self.cards[self.player_id]:
            self.player_id = (self.player_id + step) % len(self.cards)

    def _get_num_cards_to_place(self):
        '''get number of cards player needs to place before turn can be finished'''
        min_cards_to_place = self.CARDS_PER_TURN if len(self._deck) > 0 else self.CARDS_PER_TURN_EMPTY_DECK
        return max(0, min(min_cards_to_place - len(self.current_turn), len(self.cards[self.player_id])))

    def _is_any_action_possible(self):
        '''check if any player card is placable on any stack'''
        for stack in self.stacks:
            for card in self.cards[self.player_id]:
                if stack.is_placeable(card):
                    return True
        return False

    def print_turn_history(self):
        def stacks2str(stacks):
            return " ".join((f"{stack.value:>3}" for stack in stacks))

        def cards2str(cards):
            return f'[{", ".join(f"{c:>2}" for c in cards):<30}]'

        def turn2str(turn):
            return ", ".join((f"{a.card:>2}->{a.stack_id}" for a in turn))

        g = GameHandler(num_players=self.num_players, num_stacks=len(self.stacks),
                        lower_limit=self.stacks[0].lim_low, upper_limit=self.stacks[0].lim_high,
                        deck_cards=self._deck._cards.copy())
        for p, turn in self.turn_history:
            print(f'stacks: {stacks2str(g.stacks)}   cards: {cards2str(g.cards[g.player_id])}', end='')
            print(f'   turn: {turn2str(turn)}')
            g.play_turns([(p, turn)])
        print(f'stacks: {stacks2str(g.stacks)}   cards: {cards2str(g.cards[g.player_id])}')


class GameHandlerOnFire(GameHandler):
    '''special game mode where certain cards need to be covered in the next turn'''

    class EFireStatus(Enum):
        FINE = -1
        BURNED = -2

    FIRE_CARDS = [22, 33, 44, 55, 66, 77]

    def reset(self, **kwargs):
        super().reset(**kwargs)
        self._fire_states = [self.EFireStatus.FINE] * len(self.stacks)

    def get_status(self):
        result = super().get_status()
        if any(state == self.EFireStatus.BURNED for state in self._fire_states):
            result = GameStatus.LOST
        return result

    def end_turn(self):
        super().end_turn()
        for idx, stack in enumerate(self.stacks):
            if self._fire_states[idx] == stack.value:  # same value as last round
                self._fire_states[idx] = self.EFireStatus.BURNED
            elif stack.value in self.FIRE_CARDS:
                self._fire_states[idx] = stack.value
            else:
                self._fire_states[idx] = self.EFireStatus.FINE
