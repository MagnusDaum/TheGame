from enum import Enum


class CardAction:
    __slots__ = ['card', 'stack_id']

    def __init__(self, *, card, stack_id):
        self.card = card
        self.stack_id = stack_id

    def __eq__(self, other):
        return self.card == other.card and self.stack_id == other.stack_id
