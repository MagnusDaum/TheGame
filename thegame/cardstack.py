class CardStack:
    '''a card stack with lower and upper limit on which cards can be placed'''

    JUMP_VALUE = 10

    def __init__(self, lower_limit, upper_limit, incrementing, history=None):
        assert (lower_limit < upper_limit)
        self.lim_low = lower_limit
        self.lim_high = upper_limit
        self.incrementing = incrementing
        if history is not None:
            self._history = history
        else:
            self.reset()

    def reset(self):
        # store stack limits in _history, so accessing stack without any cards placed is more performant
        self._history = [self.lim_low if self.incrementing else self.lim_high]

    def __len__(self):
        return len(self._history) - 1

    @property
    def value(self):
        '''return last card without removing it from stack'''
        return self._history[-1]

    def place_card(self, card):
        '''add card to stack'''
        self._history.append(card)

    def pop_card(self):
        '''return and remove last card'''
        if len(self._history) <= 1:
            raise IndexError
        return self._history.pop()

    def is_placeable(self, card):
        '''return if given card can be placed on stack. Does not check stack limits as cards are assumed to be valid'''
        stack_top = self.value
        return (self.incrementing and (card > stack_top or card == stack_top - self.JUMP_VALUE)
                or not self.incrementing and (card < stack_top or card == stack_top + self.JUMP_VALUE))
