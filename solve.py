import argparse
from copy import deepcopy
from datetime import datetime
import os
import pickle
import random
import time

import yaml

from thegame import _solver
from thegame.game_handler import GameHandler, GameHandlerOnFire, GameStatus
from thegame.bots.bot_base import CardAction
import benchmark  # this script is basically an extension of benchmark.py


def _convert_raw_turns(turns):
    return [(p, [CardAction(stack_id=a[0], card=a[1]) for a in t]) for p, t in turns]


def _convert_py_turns(turns):
    return [(p, [(a.stack_id, a.card) for a in t]) for p, t in turns]


def _check_solve_dict(solve_dict):
    for num_players, turn_dict in sorted(solve_dict.items()):
        print(f'players: {num_players}: checking {len(turn_dict):>6} already solved games...')
        g = GameHandler(num_players=num_players)
        for game_seed, turns in turn_dict.items():
            g.load_seed(game_seed)
            g.play_turns(_convert_raw_turns(turns))
            if g.get_status() != GameStatus.WON:
                raise ValueError(f'invalid turn sequence for {num_players} players and seed {game_seed}')


def _merge_solve_dicts(lhs, rhs):
    combined_dict = deepcopy(lhs)
    for num_players, sub_dict in rhs.items():
        if not num_players in combined_dict:
            combined_dict[num_players] = deepcopy(sub_dict)
        else:
            for game_seed, turns in sub_dict.items():
                if not game_seed in combined_dict[num_players]:
                    combined_dict[num_players][game_seed] = deepcopy(turns)
    return combined_dict


def _write_solve_dict(solve_dict, result_file):
    with open(result_file + '.tmp', 'wb') as out_file:
        pickle.dump(solve_dict, out_file)
    if os.path.isfile(result_file):
        os.remove(result_file)
    os.rename(result_file + '.tmp', result_file)


def solve_game(game, *, brute_force=False, turn_width=0, duration=0, uct_c=None, multithreaded=False):
    deck = list(game._deck.cards)
    stack_values = [s.value for s in game.stacks]
    game_state = deck, stack_values, game.cards, game.player_id
    turn_length = game.CARDS_PER_TURN
    try:
        _solver.set_multithreading(multithreaded)
        if brute_force:
            turns_cpp = _solver.solve_game_brute_force(*game_state, turn_length, turn_width)
        elif duration > 0:
            if uct_c is None:
                raise ValueError('uct_c parameter required for Monte Carlo Tree Search')
            turns_cpp = _solver.solve_game_mcts(*game_state, turn_length, turn_width, uct_c, duration)
        else:
            turns_cpp = _solver.solve_game_greedy(*game_state, turn_length, turn_width)
    except NameError:
        raise RuntimeError("C++ Implementation of Solver not available!")
    return _convert_raw_turns(turns_cpp)


class PoolWorkerSolve:
    '''helper class for solving via multiprocessing.Pool'''

    def __init__(self, game, **kwargs):
        self.game = game
        self.params = kwargs

    def __call__(self, rand_seed):
        self.game.load_seed(rand_seed)
        turns = solve_game(self.game, **self.params)
        self.game.play_turns(turns)
        return rand_seed, self.game.get_status(), self.game.get_total_cards_placed(), self.game.turn_history


def main(game_seeds, *, num_players=1, result_file=None, bots=None, num_processes=None, check_file=False,
         verbose=False, **kwargs):
    num_processes = num_processes if num_processes is not None else os.cpu_count()

    game_seeds = set(game_seeds)
    seeds_solved = set()

    if result_file is not None and os.path.isfile(result_file):
        with open(result_file, 'rb') as in_file:
            solve_dict = pickle.load(in_file)
        if check_file:
            _check_solve_dict(solve_dict)
            print(f'successfully checked {result_file}\n')
        seeds_solved = set(iter(solve_dict[num_players].keys())) & game_seeds
        solve_dict = None  # clear solve_dict to reduce RAM usage
        print(f'{len(seeds_solved)} of {len(game_seeds)} games have already been solved.')
        game_seeds = game_seeds - seeds_solved

    if bots is not None:
        print(f'solving {len(game_seeds)} games...')
        game = GameHandler(num_players)
        for bot_cfg in bots:
            if not game_seeds:
                break
            benchmark.print_bot_config(bot_cfg)
            if bot_cfg["name"].lower() == "solver":
                worker = PoolWorkerSolve(game, **bot_cfg.get("params", {}))
            else:
                worker = benchmark.PoolWorker(game, bot_cfg)
            print(f'processing {len(game_seeds)} games with {game.num_players} players on {num_processes} processes')
            print(f'starting at {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}')
            start_time = time.time()
            results = benchmark.process_games(worker, game_seeds, num_processes, verbose=verbose)
            end_time = time.time()
            if results:
                benchmark.summarize_results(results, end_time-start_time)
                seeds_solved_new = set(k for k, v in results.items() if v["status"] == GameStatus.WON)
                seeds_solved |= seeds_solved_new
                game_seeds = game_seeds - seeds_solved
                if result_file is not None and seeds_solved_new:
                    print(f'\nwriting results to {result_file}...')
                    if os.path.isfile(result_file):
                        with open(result_file, 'rb') as in_file:
                            solve_dict = pickle.load(in_file)
                    else:
                        solve_dict = {num_players: {}}
                    for k, v in results.items():
                        if v["status"] == GameStatus.WON and k not in solve_dict[num_players]:
                            solve_dict[num_players][k] = _convert_py_turns(v["turns"])
                    _write_solve_dict(solve_dict, result_file)
            else:
                print('no games processed')

    seeds_unsolved = game_seeds - seeds_solved
    print(f'\nremaining seeds: {len(seeds_unsolved)}')
    return seeds_solved, seeds_unsolved


if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument('config', help='config', type=str)
    parser.add_argument('-g', '--num_games', help='number of games', type=int)
    parser.add_argument('-p', '--num_players', help='number of players', type=int)
    parser.add_argument('-n', '--num_processes', help='number of processes', type=int)
    parser.add_argument('-s', '--seed', help='random seed', type=int)
    parser.add_argument('-r', '--result_file', help='file to store turns of won games', type=str)
    parser.add_argument('--check_file', action='store_true',
                        help='check already solved games without solving any new ones')
    parser.add_argument('--verbose', action='store_true')
    args, _ = parser.parse_known_args()
    args_dict = vars(args)

    config = {}
    if "config" in args_dict:
        config_name = args_dict.pop("config")
        with open(config_name) as in_file:
            config = yaml.safe_load(in_file)
    config.update(args_dict)

    if "seed" in config:
        random.seed(config.get("seed"))
    game_seeds = config.pop("game_seeds", None)
    # set up separate random seed for each game to make results reproducible
    game_seeds = game_seeds if game_seeds is not None else random.sample(range(2**31-1), config.pop("num_games"))

    result_file = config.pop("result_file", None)
    seeds_solved, seeds_unsolved = main(game_seeds, result_file=result_file, **config)

    # save lost game seeds to new config file
    if seeds_unsolved and result_file is not None:
        output_filepath = os.path.splitext(os.path.basename(config_name))[0] + '_unsolved.yml'
        with open(output_filepath, 'w') as out_file:
            yaml.safe_dump({**config, "game_seeds": sorted(list(seeds_unsolved))}, out_file, sort_keys=False)
