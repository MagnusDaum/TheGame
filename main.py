import argparse
import time

from thegame.core import CardAction
from thegame.game_handler import GameHandler, GameStatus


def print_game(game):
    print(f'Player Cards: {game.cards[game.player_id]}')
    print(f'Stacks: {[stack.value for stack in game.stacks]}')


def do_player_turn(game):
    print_game(game)
    try:
        # while human gives valid input, add it to next move
        while True:
            card = int(input("card to place: "))
            if not card in game.cards[game.player_id]:
                print("invalid card!", end=" ")
            else:
                stack_id = int(input("place on stack: "))
                if not 0 <= stack_id < len(game.stacks):
                    print("invalid stack!", end=" ")
                else:
                    if not game.do_action(CardAction(card=card, stack_id=stack_id)):
                        print("invalid action!")
    except ValueError:  # catch empty input signaling end of turn
        pass
    game.end_turn()


def main(num_players, game_seed=None, load_path=None):
    game = GameHandler(num_players=num_players)
    game.load_seed(game_seed)
    if load_path is not None:
        game.load_from_file(load_path)

    start_time = time.time()
    while game.get_status() == GameStatus.ONGOING:
        do_player_turn(game)
    end_time = time.time()
    print(f'playing took {round(end_time-start_time, 3)} seconds')

    print_game(game)
    print("Status:", str(game.get_status()))
    print("Cards placed:", game.get_total_cards_placed())
    print("Stack Histories:")
    for stack in game.stacks:
        print(stack._history)
    print(f'Draw Pile:\n{game._deck.cards}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--load_path', help='path for loading stored game', type=str)
    parser.add_argument('-p', '--num_players', help='number of players', type=int, default=1)
    parser.add_argument('-s', '--game_seed', help='game seed', type=int)
    args, _ = parser.parse_known_args()

    main(game_seed=args.game_seed, load_path=args.load_path, num_players=args.num_players)
