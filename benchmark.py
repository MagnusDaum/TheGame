import argparse
from datetime import datetime
import importlib
import inspect
import multiprocessing
import os
import random
import time
import sys

import numpy as np
import yaml

from thegame.game_handler import GameHandler, GameHandlerOnFire, GameStatus
from thegame.bots.bot_base import BotBase


def _load_bot_class(name, module, **kwargs):
    # import bot module
    module_name = module.split('.')[-1]
    module = importlib.import_module(module)
    sys.modules[module_name] = module
    # find BotBase subclasses in module
    members = inspect.getmembers(module, lambda m: inspect.isclass(m)
                                 and issubclass(m, BotBase) and m != BotBase)
    bot_class = next((m[1] for m in members if m[0] == name))
    return bot_class


def print_bot_config(bot_cfg):
    bot_name = bot_cfg["name"]
    bot_params = bot_cfg.get("params", {})
    bot_params_str = '' if not bot_params else ' (' + ', '.join(f'{k}={v}' for k, v in bot_params.items()) + ')'
    print(f'\n### {bot_name}{bot_params_str} ###')


class PoolWorker:
    '''helper class for benchmarking via multiprocessing.Pool'''

    def __init__(self, game, bot_config):
        self.game = game
        bot_class = _load_bot_class(**bot_config)
        self.players = [bot_class(**bot_config.get("params", {})) for _ in range(game.num_players)]

    def __call__(self, rand_seed):
        self.game.load_seed(rand_seed)
        for player in self.players:
            player.reset()
            if hasattr(player, "seed"):
                player.seed(rand_seed)
        self.game.play(self.players)
        return rand_seed, self.game.get_status(), self.game.get_total_cards_placed(), self.game.turn_history


def process_games(worker, game_seeds, num_processes, verbose=False):
    results = {}
    try:
        if num_processes == 1:
            # workaround if bot cannot be pickled (necessary for multiprocessing.Pool)
            for rand_seed in game_seeds:
                res = worker(rand_seed)
                results[res[0]] = {"status": res[1], "cards_placed": res[2], "turns": res[3]}
                if verbose:
                    print(f'''{datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'''
                          f'''  seed: {res[0]:>10}: {res[1].name:^7} cards placed: {res[2]:>3}''')
        else:
            with multiprocessing.Pool(num_processes) as pool:
                for res in pool.imap_unordered(worker, game_seeds):
                    results[res[0]] = {"status": res[1], "cards_placed": res[2], "turns": res[3]}
                    if verbose:
                        print(f'''{datetime.now().strftime("%Y-%m-%d %H:%M:%S")}'''
                              f'''  seed: {res[0]:>10}: {res[1].name:^7} cards placed: {res[2]:>3}''')
    except KeyboardInterrupt:
        pass
    return results


def summarize_results(results, duration):
    num_games = len(results)
    num_wins = sum(v["status"] == GameStatus.WON for v in results.values())
    cards_placed = [v["cards_placed"] for v in results.values()]
    print(f'''processing {num_games} games took {round(duration, 3)} seconds'''
          f''' ({(duration) / num_games:.3f} sec/game)''')
    print(f'wins: {num_wins} ({num_wins * 100.0 / num_games:.2f}%)')
    print(f'mean cards placed: {np.mean(cards_placed):.2f}')
    print(f'''(min: {np.min(cards_placed)} max: {np.max(cards_placed)} std: {np.std(cards_placed):.2f}'''
          f''' cards/sec: {np.sum(cards_placed) / (duration):.0f})''')


def main(game_seeds, *, bots, num_players, num_processes=None, incremental=False, turn_file=None, verbose=False, **kwargs):
    num_processes = num_processes if num_processes is not None else os.cpu_count()

    game = GameHandler(num_players)
    for bot_cfg in bots:
        print_bot_config(bot_cfg)
        worker = PoolWorker(game, bot_cfg)
        print(f'processing {len(game_seeds)} games with {num_players} players on {num_processes} processes...')
        start_time = time.time()
        results = process_games(worker, game_seeds, num_processes, verbose=verbose)
        end_time = time.time()
        if results:
            summarize_results(results, end_time-start_time)
        else:
            print('no games processed')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument('config', help='config', type=str)
    parser.add_argument('-g', '--num_games', help='number of games', type=int)
    parser.add_argument('-p', '--num_players', help='number of players', type=int)
    parser.add_argument('-n', '--num_processes', help='number of processes', type=int)
    parser.add_argument('-s', '--seed', help='random seed', type=int)
    parser.add_argument('--verbose', action='store_true')
    args, _ = parser.parse_known_args()
    args_dict = vars(args)

    config = {}
    if "config" in args_dict:
        config_name = args_dict.pop("config")
        with open(config_name) as in_file:
            config = yaml.safe_load(in_file)
    config.update(args_dict)

    if "seed" in config:
        random.seed(config.get("seed"))
    game_seeds = config.pop("game_seeds", None)
    # set up separate random seed for each game to make results reproducible
    game_seeds = game_seeds if game_seeds is not None else random.sample(range(2**31-1), config.pop("num_games"))

    main(game_seeds, **config)
