import asyncio
import tkinter as tk
from tkinter import filedialog, simpledialog

from thegame.core import CardAction
from thegame.game_handler import GameHandler, GameStatus
from thegame.bots.bot_heuristic import BotHeuristic


class TheGameApp(tk.Frame):
    def __init__(self, game, frame_master=None):
        super().__init__(frame_master)
        self.master = frame_master
        self.pack(expand=True, fill=tk.BOTH)
        self.create_widgets()

        # event handling
        self.dragged_card = None
        self.canv.bind('<ButtonPress-1>', self.on_canvas_press)
        self.canv.bind('<B1-Motion>', self.on_canvas_motion)
        self.canv.bind('<ButtonRelease-1>', self.on_canvas_release)

        # mapping of canvas elements to card/stack indices
        self.canv_map = {}

        self.game = game

    def create_widgets(self):
        self.gamestate_label = tk.Label(self)
        self.gamestate_label.grid(row=0, column=0)

        self.cardsplaced_label = tk.Label(self)
        self.cardsplaced_label.grid(row=0, column=1)

        self.cardsleft_label = tk.Label(self)
        self.cardsleft_label.grid(row=0, column=2)

        self.canv = tk.Canvas(self, bg='#080')
        self.canv.grid(row=1, column=0, columnspan=6, sticky='NSWE')

        self.load_seed_btn = tk.Button(self, text="Load Seed", command=self.load_seed)
        self.load_seed_btn.grid(row=2, column=0)

        self.load_game_btn = tk.Button(self, text="Load Game", command=self.load_game)
        self.load_game_btn.grid(row=2, column=1)

        self.save_game_btn = tk.Button(self, text="Save Game", command=self.save_game)
        self.save_game_btn.grid(row=2, column=2)

        self.new_game_btn = tk.Button(self, text="New Game", command=self.new_game)
        self.new_game_btn.grid(row=2, column=3)

        self.submit_turn_btn = tk.Button(self, text="Submit Turn", command=self.submit_turn, state="disabled")
        self.submit_turn_btn.grid(row=2, column=4)

        self.revert_action_btn = tk.Button(self, text="Revert", command=self.revert_action, state="disabled")
        self.revert_action_btn.grid(row=2, column=5)

        self.rowconfigure(1, weight=1)
        # workaround to get canvas spanning whole window in linux and windows
        # (setting width to winfo_width() after update_idletasks() did not work in windows)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)
        self.columnconfigure(3, weight=1)
        self.columnconfigure(4, weight=100)
        self.columnconfigure(5, weight=30)

    def on_canvas_press(self, event):
        items = self.canv.find_overlapping(event.x, event.y, event.x, event.y)
        card_items = [i for i in items if i in self.canv.find_withtag('card_border')]
        if card_items:
            item_idx = self.canv_map[card_items[0]]
            self.dragged_card = next((i for i in self.canv.find_withtag('card')
                                      if str(item_idx) in self.canv.gettags(i)))
            self.drag_orig = self.canv.coords(self.dragged_card)
            self.drag_offset = [self.drag_orig[0]-event.x, self.drag_orig[1]-event.y]

    def on_canvas_motion(self, event):
        if self.dragged_card is not None:
            coords = self.canv.coords(self.dragged_card)
            # calculate relative move as canvas.moveto() doesnt work as expected
            new_x = event.x + self.drag_offset[0] - coords[0]
            new_y = event.y + self.drag_offset[1] - coords[1]
            self.canv.move(self.dragged_card, new_x, new_y)

    def on_canvas_release(self, event):
        if self.dragged_card is not None:
            invalid_drop = True  # reset card to original position if drop was invalid
            items = self.canv.find_overlapping(event.x, event.y, event.x, event.y)
            stack_items = [i for i in items if i in self.canv.find_withtag('stack_border')]
            if stack_items:
                stack_id = self.canv_map[stack_items[0]]
                card_idx = self.canv_map[self.dragged_card]
                invalid_drop = not self.add_action(card_idx, stack_id)
            if invalid_drop:
                coords = self.canv.coords(self.dragged_card)
                self.canv.move(self.dragged_card, self.drag_orig[0] - coords[0], self.drag_orig[1] - coords[1])
            self.dragged_card = None

    def draw_game(self):
        self.submit_turn_btn["state"] = "normal" if self.game.can_end_turn() else "disabled"
        self.revert_action_btn["state"] = "normal" if self.game.current_turn else "disabled"
        self.gamestate_label.config(text=f'Game State: {self.game.get_status().name}')
        self.cardsplaced_label.config(text=f'Cards Placed: {self.game.get_total_cards_placed()}')
        self.cardsleft_label.config(text=f'Cards Left: {self.game.get_total_cards_left()}')
        self.draw_stacks(self.game.stacks)
        self.draw_hand_cards(self.game.cards[self.game.player_id])
        self.canv.update()

    def draw_stacks(self, stacks):
        self.canv.delete('stack')
        self.canv.delete('stack_border')
        w = self.canv.winfo_width()
        h = self.canv.winfo_height()
        w_stack = int(w * (5/6) / len(stacks))
        start_x = int(w / 2 - w_stack * (len(stacks)-1) / 2)
        h_stack = h // 4
        y = h // 2
        y0 = y - h_stack // 2
        y1 = y + h_stack // 2
        for idx, st in enumerate(stacks):
            x = start_x + idx * w_stack
            r = self.canv.create_rectangle(x - w_stack // 2, y0, x + w_stack // 2,
                                           y1, fill='#060', tag=['stack_border', idx])
            self.canv_map[r] = idx
            if st.incrementing:
                self.canv.create_line(x - w_stack // 2, y0, x, y0 - h_stack // 4, fill='#060')
                self.canv.create_line(x + w_stack // 2, y0, x, y0 - h_stack // 4, fill='#060')
            else:
                self.canv.create_line(x - w_stack // 2, y1, x, y1 + h_stack // 4, fill='#060')
                self.canv.create_line(x + w_stack // 2, y1, x, y1 + h_stack // 4, fill='#060')
            t = self.canv.create_text(x, y, text=str(st.value), font=(
                'Arial', h_stack // 2), fill='#fff', tag=['stack', idx])
            self.canv_map[t] = idx

    def draw_hand_cards(self, cards):
        self.canv.delete('card')
        self.canv.delete('card_border')
        if len(cards) == 0:
            return
        w = self.canv.winfo_width()
        h = self.canv.winfo_height()
        w_card = int(w * (5/6) / len(cards))
        start_x = int(w / 2 - w_card * (len(cards)-1) / 2)
        h_card = h // 6
        y = int(min(h * 9/10, h - h_card / 2))
        for idx, st in enumerate(cards):
            x = start_x + idx * w_card
            r = self.canv.create_rectangle(x - w_card // 2, y - h_card // 2, x + w_card //
                                           2, y + h_card // 2, fill='#060', tag=['card_border', idx])
            self.canv_map[r] = idx
            t = self.canv.create_text(x, y, text=str(st), font=('Arial', h_card // 2), fill='#ff0', tag=['card', idx])
            self.canv_map[t] = idx

    ###########################################################################
    # game logic

    def new_game(self):
        self.canv.delete("all")
        self.game.reset(new_deck=True)
        self.draw_game()

    def load_seed(self):
        seed = simpledialog.askinteger(title="load seed", prompt="specify game seed")
        if seed is not None:
            self.game.load_seed(seed)
            self.canv.delete("all")
            self.draw_game()

    def load_game(self):
        file_path = filedialog.askopenfilename(title="select game file", filetypes=(("yaml files", "*.yml"),))
        self.game.load_from_file(file_path)
        self.canv.delete("all")
        self.draw_game()

    def save_game(self):
        file_path = filedialog.asksaveasfilename(title="select game file", filetypes=(("yaml files", "*.yml"),))
        self.game.save(file_path)

    def revert_action(self):
        if self.game.undo_action():
            self.draw_game()

    def submit_turn(self):
        if self.game.end_turn():
            self.draw_game()

    def add_action(self, card_idx, stack_id):
        if self.game is not None:
            card = self.game.cards[self.game.player_id][card_idx]
            action = CardAction(card=card, stack_id=stack_id)
            if self.game.do_action(action):
                self.draw_game()
                return True
        return False


def main(num_players):
    root = tk.Tk()
    root.title('TheGame')
    root.geometry(f'{root.winfo_screenwidth()//2}x{root.winfo_screenheight()//2}')
    game = GameHandler(num_players=num_players)
    app = TheGameApp(frame_master=root, game=game)
    app.mainloop()


if __name__ == '__main__':
    main(num_players=1)
