#include "solve.hpp"

#include "thegame/mcts.hpp"

#include <algorithm>

namespace thegame
{
bool ENABLE_PARALLELISM = false;

namespace
{
turnseq_t solve_game_recursive(GameState &f_gamestate, int f_turn_length, int f_turn_width)
{
    if (f_gamestate.deck_empty())
    {
        f_turn_length = TURN_LENGTH_MIN;
    }
    const auto all_turns = f_gamestate.generate_turns(f_turn_length, f_turn_width);
    if (all_turns.empty())
    {
        return {};
    }

    const auto state_copy   = GameState(f_gamestate);
    auto       best_turnseq = turnseq_t{};
    for (const auto &turn : all_turns)
    {
        f_gamestate.apply_turn(turn);
        // check for end of game
        if (f_gamestate.count_remaining_cards() == 0)
        {
            best_turnseq = {{state_copy.get_player(), turn}};
            break;
        }
        // otherwise recurse further
        const auto turn_seq = solve_game_recursive(f_gamestate, f_turn_length, f_turn_width);
        if (turn_seq.size() >= best_turnseq.size())
        {
            best_turnseq = turn_seq;
            best_turnseq.emplace_back(state_copy.get_player(), turn);
        }
        if (f_gamestate.count_remaining_cards() == 0)
        {
            break;  // game won in recursion
        }

        // if game was not solved revert current turn
        f_gamestate = state_copy;
    }
    return best_turnseq;
}

}  // namespace

turnseq_t solve_game_brute_force(GameState &f_gamestate, int f_turn_length, int f_turn_width)
{
    auto turn_sequence = solve_game_recursive(f_gamestate, f_turn_length, f_turn_width);
    std::reverse(turn_sequence.begin(), turn_sequence.end());
    if (f_gamestate.count_remaining_cards() > 0)
    {
        // if game was not fully solved, gamestate was reset --> apply turns again
        for (const auto &t : turn_sequence)
        {
            f_gamestate.apply_turn(std::get<1>(t));
        }
        // in case a full turn is not possible anymore, still try to find best sub-turn
        while (f_turn_length > TURN_LENGTH_MIN)
        {
            --f_turn_length;
            const auto subturnseq = solve_game_recursive(f_gamestate, f_turn_length, f_turn_width);
            if (!subturnseq.empty())
            {
                turn_sequence.push_back(subturnseq[subturnseq.size() - 1]);  // last as seq is reversed
                break;
            }
        }
    }
    return turn_sequence;
}

turnseq_t solve_game_greedy(GameState &f_gamestate, int f_turn_length, int f_turn_width)
{
    auto turn_sequence = turnseq_t{};
    while (true)
    {
        if (f_gamestate.deck_empty())
        {
            f_turn_length = TURN_LENGTH_MIN;
        }
        int        best_turn_id = 0;
        const auto all_turns    = f_gamestate.generate_turns(f_turn_length, f_turn_width);
        if (all_turns.empty())
        {
            break;
        }
        else if (all_turns.size() > 1)
        {
            // evaluate turns via rollout
            auto best_reward = std::numeric_limits<float>::lowest();
            for (int turn_id = 0; turn_id < static_cast<int>(all_turns.size()); ++turn_id)
            {
                auto gamestate = GameState(f_gamestate);
                gamestate.apply_turn(all_turns[turn_id]);
                const auto reward = gamestate.rollout(f_turn_length);
                if (reward > best_reward)
                {
                    best_reward  = reward;
                    best_turn_id = turn_id;
                }
            }
        }
        turn_sequence.emplace_back(f_gamestate.get_player(), all_turns[best_turn_id]);
        f_gamestate.apply_turn(all_turns[best_turn_id]);
    }
    // in case a full turn is not possible anymore, still try to find best sub-turn
    while (f_turn_length > TURN_LENGTH_MIN)
    {
        --f_turn_length;
        const auto subturnseq = solve_game_greedy(f_gamestate, f_turn_length, f_turn_width);
        if (!subturnseq.empty())
        {
            turn_sequence.push_back(subturnseq[0]);
            break;
        }
    }
    return turn_sequence;
}

turnseq_t solve_game_mcts(GameState &               f_gamestate,
                          int                       f_turn_length,
                          int                       f_turn_width,
                          float                     f_uct_c,
                          std::chrono::milliseconds f_search_duration)
{
    const auto end_time = std::chrono::system_clock::now() + f_search_duration;

    auto tree = mcts::Tree(f_gamestate, f_turn_length, f_turn_width, f_uct_c, ENABLE_PARALLELISM);
    while (std::chrono::system_clock::now() < end_time)
    {
        auto       gamestate = GameState(f_gamestate);
        const bool is_solved = tree.add_node(gamestate);
        if (is_solved) break;
    }

    // create turn sequence
    auto turn_sequence = turnseq_t{};
    auto mcts_turns    = tree.get_turns();
    for (auto &&t : mcts_turns)
    {
        turn_sequence.emplace_back(f_gamestate.get_player(), t);
        f_gamestate.apply_turn(t);
    }
    auto followup_turns = solve_game_greedy(f_gamestate, f_turn_length, f_turn_width);
    turn_sequence.insert(turn_sequence.end(),
                         std::make_move_iterator(followup_turns.begin()),
                         std::make_move_iterator(followup_turns.end()));
    return turn_sequence;
}

}  // namespace thegame