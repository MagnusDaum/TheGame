#ifndef THEGAME_SOLVE_HPP
#define THEGAME_SOLVE_HPP

#include "thegame/core.hpp"
#include "thegame/gamestate.hpp"

#include <chrono>
#include <tuple>
#include <vector>

namespace thegame
{
// flag for enabling multithreading (to avoid duplication with python-side multithreading)
// disabled by default
extern bool ENABLE_PARALLELISM;

// vector of "turns". each turn contains player id and 1+ moves
using turnseq_t = std::vector<std::tuple<int, std::vector<CardMove>>>;

// check all possible turns to check if any lead to a won game
// if turn_width > 0, limit "search width" per recursion level to turn_width turns
turnseq_t solve_game_brute_force(GameState& f_gamestate, int f_turn_length, int f_turn_width);

// always choose the turn with the best rollout to see if this leads to a won game
turnseq_t solve_game_greedy(GameState& f_gamestate, int f_turn_length, int f_turn_width);

// use Monte Carlo Tree search to find solution
turnseq_t solve_game_mcts(GameState&                f_gamestate,
                          int                       f_turn_length,
                          int                       f_turn_width,
                          float                     f_uct_c,
                          std::chrono::milliseconds f_search_duration);
}  // namespace thegame

#endif
