#include "solve.hpp"

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace thegame
{
namespace
{
using pyturnseq_t = std::vector<std::tuple<int, std::vector<pymove_t>>>;

pyturnseq_t convert_turnseq(turnseq_t &f_turnseq)
{
    auto result = pyturnseq_t{};
    for (auto iter = f_turnseq.begin(); iter != f_turnseq.end(); ++iter)
    {
        const auto &player_id = std::get<0>(*iter);
        auto &      cpp_moves = std::get<1>(*iter);
        auto        res       = std::vector<pymove_t>{};
        std::move(cpp_moves.begin(), cpp_moves.end(), std::back_inserter(res));
        result.emplace_back(player_id, res);
    }
    return result;
}

pyturnseq_t py_solve_game_brute_force(std::vector<int> &             f_deck,
                                      std::vector<int> &             f_stacks,
                                      std::vector<std::vector<int>> &f_cards,
                                      int                            f_player_id,
                                      int                            f_turn_length,
                                      int                            f_turn_width)
{
    auto gamestate = GameState(DeckViewer(f_deck), f_stacks, f_cards, f_player_id);
    auto turnseq   = solve_game_brute_force(gamestate, f_turn_length, f_turn_width);
    return convert_turnseq(turnseq);
}

pyturnseq_t py_solve_game_greedy(std::vector<int> &             f_deck,
                                 std::vector<int> &             f_stacks,
                                 std::vector<std::vector<int>> &f_cards,
                                 int                            f_player_id,
                                 int                            f_turn_length,
                                 int                            f_turn_width)
{
    auto gamestate = GameState(DeckViewer(f_deck), f_stacks, f_cards, f_player_id);
    auto turnseq   = solve_game_greedy(gamestate, f_turn_length, f_turn_width);
    return convert_turnseq(turnseq);
}

pyturnseq_t py_solve_game_mcts(std::vector<int> &             f_deck,
                               std::vector<int> &             f_stacks,
                               std::vector<std::vector<int>> &f_cards,
                               int                            f_player_id,
                               int                            f_turn_length,
                               int                            f_turn_width,
                               float                          f_uct_c,
                               float                          f_search_time_seconds)
{
    const auto duration  = std::chrono::milliseconds(static_cast<int>(f_search_time_seconds * 1000.0F));
    auto       gamestate = GameState(DeckViewer(f_deck), f_stacks, f_cards, f_player_id);
    auto       turnseq   = solve_game_mcts(gamestate, f_turn_length, f_turn_width, f_uct_c, duration);
    return convert_turnseq(turnseq);
}

}  // namespace
}  // namespace thegame

PYBIND11_MODULE(_solver, m)  // add _ prefix to make distinguishable from python module
{
    m.doc() = "Solver for The Game";

    m.def("solve_game_brute_force", &thegame::py_solve_game_brute_force, "solve game via brute force");
    m.def("solve_game_greedy", &thegame::py_solve_game_greedy, "solve game by greedily choosing next turn");
    m.def("solve_game_mcts", &thegame::py_solve_game_mcts, "solve game via Monte Carlo Tree Search");

    m.def(
        "set_multithreading", [](bool mt) { thegame::ENABLE_PARALLELISM = mt; }, "enable/disable multithreading");
}
