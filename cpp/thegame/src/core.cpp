#include "thegame/core.hpp"

#include <algorithm>
#include <cmath>

namespace thegame
{
namespace
{
// check if card can be placed on stack
bool is_placeable(int card, int stack_value, bool stack_incrementing)
{
    return (stack_incrementing && (card > stack_value || card == stack_value - JUMP_VALUE)) ||
           (!stack_incrementing && (card < stack_value || card == stack_value + JUMP_VALUE));
}

// sort turn by affected stacks
void sort_moves(std::vector<CardMove> &f_moves)
{
    const auto move_comparator = [](const CardMove &f_lhs, const CardMove &f_rhs) {
        return f_lhs.stack_id < f_rhs.stack_id;
    };
    std::stable_sort(f_moves.begin(), f_moves.end(), move_comparator);
}

// assess stacks based on their remaining "free space"
int assess_stacks(const std::vector<int> &f_stacks)
{
    int value = 0;
    for (int stack_id = 0; stack_id < static_cast<int>(f_stacks.size()); ++stack_id)
    {
        const bool incrementing = stack_id < static_cast<int>(f_stacks.size() / 2);
        value += incrementing ? STACK_HIGH + STACK_LOW - f_stacks[stack_id] : f_stacks[stack_id];
    }
    return value;
}

void generate_all_turns_recursive(std::vector<std::vector<CardMove>> &f_turns,
                                  std::vector<CardMove> &             f_current_turn,
                                  std::vector<int> &                  f_stacks,
                                  std::vector<int> &                  f_cards,
                                  int                                 f_turn_length)
{
    for (int stack_id = 0; stack_id < static_cast<int>(f_stacks.size()); ++stack_id)
    {
        const bool incrementing = stack_id < static_cast<int>(f_stacks.size() / 2);
        for (int card_id = 0; card_id < static_cast<int>(f_cards.size()); ++card_id)
        {
            if (is_placeable(f_cards[card_id], f_stacks[stack_id], incrementing))
            {
                f_current_turn.emplace_back(stack_id, f_cards[card_id]);

                if (f_turn_length > 1)
                {
                    const auto tmp_val = f_stacks[stack_id];
                    f_stacks[stack_id] = f_cards[card_id];
                    f_cards.erase(f_cards.begin() + card_id);
                    generate_all_turns_recursive(f_turns, f_current_turn, f_stacks, f_cards, f_turn_length - 1);
                    f_cards.insert(f_cards.begin() + card_id, f_stacks[stack_id]);
                    f_stacks[stack_id] = tmp_val;
                }
                else
                {
                    f_turns.push_back(f_current_turn);
                }

                f_current_turn.pop_back();
            }
        }
    }
}

// instead of generating all turns and filtering/evaluating them afterwards, filter during generation
void generate_best_turns_recursive(std::vector<std::vector<CardMove>> &f_turns,
                                   std::vector<CardMove> &             f_current_turn,
                                   std::vector<int> &                  f_stacks,
                                   std::vector<int> &                  f_cards,
                                   int                                 f_turn_length,
                                   std::vector<int> &                  f_turn_values)
{
    for (int stack_id = 0; stack_id < static_cast<int>(f_stacks.size()); ++stack_id)
    {
        const bool incrementing = stack_id < static_cast<int>(f_stacks.size() / 2);
        for (int card_id = 0; card_id < static_cast<int>(f_cards.size()); ++card_id)
        {
            if (is_placeable(f_cards[card_id], f_stacks[stack_id], incrementing))
            {
                // do move
                f_current_turn.emplace_back(stack_id, f_cards[card_id]);
                const auto tmp_val = f_stacks[stack_id];
                f_stacks[stack_id] = f_cards[card_id];
                f_cards.erase(f_cards.begin() + card_id);

                // extend or evaluate turn depending on recursion depth
                if (f_turn_length > 1)
                {
                    generate_best_turns_recursive(
                        f_turns, f_current_turn, f_stacks, f_cards, f_turn_length - 1, f_turn_values);
                }
                else
                {
                    // compute value of move via most "stack space"
                    const int move_value = assess_stacks(f_stacks);
                    // insert move if it is better than an existing one
                    int insert_idx = 0;
                    for (; insert_idx < static_cast<int>(f_turn_values.size()); ++insert_idx)
                    {
                        if (move_value == f_turn_values[insert_idx])  // possible duplicate
                        {
                            // check if current move is equivalent to an existing one
                            // -> both need to be sorted first
                            sort_moves(f_turns[insert_idx]);
                            // do not modify f_current_turn as this will break this function
                            auto tmp_move = std::vector<CardMove>(f_current_turn);
                            sort_moves(tmp_move);
                            if (std::equal(f_turns[insert_idx].begin(), f_turns[insert_idx].end(), tmp_move.begin()))
                            {
                                // move is a duplicate -> do not insert
                                break;
                            }
                        }

                        if (move_value > f_turn_values[insert_idx])
                        {
                            f_turn_values.insert(f_turn_values.begin() + insert_idx, move_value);
                            f_turn_values.pop_back();
                            f_turns.insert(f_turns.begin() + insert_idx, f_current_turn);
                            f_turns.pop_back();
                            break;
                        }
                    }
                }
                // revert move
                f_cards.insert(f_cards.begin() + card_id, f_stacks[stack_id]);
                f_stacks[stack_id] = tmp_val;
                f_current_turn.pop_back();
            }
        }
    }
}

}  // namespace

std::vector<std::vector<CardMove>> generate_turns(const std::vector<int> &f_stacks,
                                                  const std::vector<int> &f_cards,
                                                  int                     f_turn_length,
                                                  int                     f_num_turns)
{
    auto turns = std::vector<std::vector<CardMove>>(f_num_turns, std::vector<CardMove>{});

    auto stacks       = std::vector<int>(f_stacks);
    auto cards        = std::vector<int>(f_cards);
    auto current_move = std::vector<CardMove>{};

    if (f_num_turns <= 0)  // generate unfiltered
    {
        generate_all_turns_recursive(turns, current_move, stacks, cards, f_turn_length);
    }
    else
    {
        constexpr auto turn_value_default = std::numeric_limits<int>::min();
        auto           turn_values        = std::vector<int>(f_num_turns, turn_value_default);
        generate_best_turns_recursive(turns, current_move, stacks, cards, f_turn_length, turn_values);
        // remove invalid turns (occurs if less than f_num_turns are possible)
        int erase_idx = 0;
        for (; erase_idx < static_cast<int>(turn_values.size()); ++erase_idx)
        {
            if (turn_values[erase_idx] == turn_value_default)
            {
                turns.erase(turns.begin() + erase_idx, turns.end());
                break;
            }
        }
    }

    return turns;
}

}  // namespace thegame
