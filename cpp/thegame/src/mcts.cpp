#include "thegame/mcts.hpp"

#include "omp.h"

#include <algorithm>
#include <cmath>
#include <iostream>  // TODO debug
#include <numeric>

namespace thegame
{
namespace mcts
{
class Tree::Node
{
   public:
    Node(std::vector<std::vector<CardMove>> &&f_turns, Node *f_parent) : m_parent(f_parent), m_turns(f_turns)
    {
        m_rewards = std::vector<float>(m_turns.size(), 0.0F);
        m_visits  = std::vector<float>(m_turns.size(), 0.0F);
    }

    Node *                             m_parent   = nullptr;
    std::vector<std::unique_ptr<Node>> m_children = {};
    std::vector<std::vector<CardMove>> m_turns    = {};
    std::vector<float>                 m_rewards  = {};
    std::vector<float>                 m_visits   = {};
};

namespace
{
int compute_size(const Tree::Node *f_node)
{
    if (f_node == nullptr) return 0;

    int count = 1;
    for (auto &c : f_node->m_children)
    {
        if (c) count += compute_size(c.get());
    }
    return count;
}

int compute_height(const Tree::Node *f_node)
{
    if (f_node == nullptr) return 0;

    int depth = 1;
    for (auto &c : f_node->m_children)
    {
        if (c) depth = std::max(depth, 1 + compute_height(c.get()));
    }
    return depth;
}

int get_child_index(Tree::Node *f_node)
{
    for (auto iter = f_node->m_parent->m_children.begin(); iter != f_node->m_parent->m_children.end(); ++iter)
    {
        if (iter->get() == f_node)
        {
            return static_cast<int>(std::distance(f_node->m_parent->m_children.begin(), iter));
        }
    }
    throw std::runtime_error("invalid monte carlo tree!");
}

bool assess_turns(Tree::Node &f_node, const GameState &f_gamestate, int f_turn_length, bool f_multithreaded)
{
    bool solved = false;
    #pragma omp parallel for if (f_multithreaded)
    for (int turn_id = 0; turn_id < static_cast<int>(f_node.m_turns.size()); ++turn_id)
    {
        auto gamestate = GameState(f_gamestate);
        gamestate.apply_turn(f_node.m_turns[turn_id]);
        const auto reward = gamestate.rollout(f_turn_length);
        #pragma omp critical
        if (!solved)
        {
            if (reward < REWARD_WON)
            {
                ++f_node.m_visits[turn_id];
                f_node.m_rewards[turn_id] += reward;
            }
            else
            {
                // backpropagate maximum reward
                f_node.m_visits[turn_id]  = std::numeric_limits<float>::max() / REWARD_WON;
                f_node.m_rewards[turn_id] = std::numeric_limits<float>::max();
                auto *node                = &f_node;
                while (node->m_parent != nullptr)
                {
                    const int child_idx                  = get_child_index(node);
                    node->m_parent->m_visits[child_idx]  = std::numeric_limits<float>::max() / REWARD_WON;
                    node->m_parent->m_rewards[child_idx] = std::numeric_limits<float>::max();
                    node                                 = node->m_parent;
                }
                solved = true;
            }
        }
    }
    return solved;
}
}  // namespace

Tree::Tree(const GameState &f_gamestate, int f_turn_length, int f_turn_width, float f_uct_c, bool f_multithreaded)
    : m_root(std::make_unique<Node>(
          f_gamestate.generate_turns(f_gamestate.deck_empty() ? TURN_LENGTH_MIN : f_turn_length, f_turn_width),
          nullptr)),
      m_turn_length(f_turn_length),
      m_turn_width(f_turn_width),
      m_uct_c(f_uct_c),
      m_multithreaded(f_multithreaded)
{
    assess_turns(*m_root, f_gamestate, m_turn_length, m_multithreaded);
}

Tree::~Tree()       = default;
Tree::Tree(Tree &&) = default;
Tree &Tree::operator=(Tree &&) = default;

bool Tree::add_node(GameState &f_gamestate)
{
    // selection
    auto *node      = m_root.get();
    int   child_idx = 0;
    while (true)
    {
        child_idx = select_turn_uct(node->m_rewards, node->m_visits, m_uct_c);
        f_gamestate.apply_turn(node->m_turns[child_idx]);
        if (child_idx >= static_cast<int>(node->m_children.size()))
        {
            break;
        }
        node = node->m_children[child_idx].get();
    }

    // expansion
    const int turn_length    = f_gamestate.deck_empty() ? TURN_LENGTH_MIN : m_turn_length;
    auto      possible_turns = f_gamestate.generate_turns(turn_length, m_turn_width);
    if (!possible_turns.empty())
    {
        const auto new_idx = static_cast<int>(node->m_children.size());
        if (new_idx != child_idx)
        {
            std::swap(node->m_turns[child_idx], node->m_turns[new_idx]);
            std::swap(node->m_rewards[child_idx], node->m_rewards[new_idx]);
            std::swap(node->m_visits[child_idx], node->m_visits[new_idx]);
            child_idx = new_idx;
        }
        node->m_children.push_back(std::make_unique<Node>(std::move(possible_turns), node));
        node                 = node->m_children.back().get();
        const bool is_solved = assess_turns(*node, f_gamestate, m_turn_length, m_multithreaded);
        if (is_solved)
        {
            return true;
        }
    }
    else
    {
        // increase visits of non-extended node to change its probability to get selected
        const auto mean_reward = node->m_rewards[child_idx] / node->m_visits[child_idx];
        if (mean_reward > REWARD_WON - 1e-3F)  // account for float inaccuracy
        {
            return true;
        }
        node->m_visits[child_idx] += static_cast<float>(m_turn_width);
        node->m_rewards[child_idx] += mean_reward * static_cast<float>(m_turn_width);
    }

    // backpropagation
    const auto sum_visits  = std::accumulate(node->m_visits.begin(), node->m_visits.end(), 0.0F);
    const auto sum_rewards = std::accumulate(node->m_rewards.begin(), node->m_rewards.end(), 0.0F);
    while (node->m_parent != nullptr)
    {
        child_idx = get_child_index(node);
        node->m_parent->m_visits[child_idx] += sum_visits;
        node->m_parent->m_rewards[child_idx] += sum_rewards;
        node = node->m_parent;
    }
    return false;
}

std::vector<std::vector<CardMove>> Tree::get_turns() const
{
    // const auto selector = [](float r, float v) {
    //     return (v > 0.0F) ? (r / v) : std::numeric_limits<float>::lowest();
    // };
    const auto selector = [](float, float v) { return v; };

    auto        turns = std::vector<std::vector<CardMove>>{};
    const auto *node  = m_root.get();
    while (true)
    {
        const int best_turn_id = select_turn(node->m_rewards, node->m_visits, selector);
        turns.push_back(node->m_turns[best_turn_id]);
        if (best_turn_id >= static_cast<int>(node->m_children.size()))
        {
            break;
        }
        node = node->m_children[best_turn_id].get();
    }
    return turns;
}

int Tree::size() const { return compute_size(m_root.get()); }

int Tree::height() const { return compute_height(m_root.get()); }

void Tree::_debug_print() const
{
    std::cout << "mcts tree: size: " << size() << " height: " << height() << "\n";
    for (int i = 0; i < static_cast<int>(m_root->m_children.size()); ++i)
    {
        std::cout << "child: " << i << " visits: " << m_root->m_visits[i]
                  << " mean reward: " << m_root->m_rewards[i] / std::max(1.0F, m_root->m_visits[i])
                  << " size: " << compute_size(m_root->m_children[i].get())
                  << " height: " << compute_height(m_root->m_children[i].get()) << "\n";
    }
}

int select_turn_uct(const std::vector<float> &f_rewards, const std::vector<float> &f_visits, float f_uct_c)
{
    // UCT (Upper Confidence bound applied to Trees) formula from MCTS
    const auto sum_visits   = std::log(std::accumulate(f_visits.begin(), f_visits.end(), 0.0F));
    const auto uct_selector = [sum_visits, f_uct_c](float r, float v) {
        return r / v + f_uct_c * std::sqrt(sum_visits / v);
    };
    const auto turn_id = select_turn(f_rewards, f_visits, uct_selector);
    return turn_id;
}

}  // namespace mcts
}  // namespace thegame
