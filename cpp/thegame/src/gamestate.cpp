#include "thegame/gamestate.hpp"

#include <algorithm>

namespace thegame
{
namespace
{
void do_move(std::vector<int> &f_stacks, std::vector<int> &f_cards, const CardMove &f_move)
{
    f_stacks[f_move.stack_id] = f_move.card;
    const auto card_pos       = std::find(f_cards.begin(), f_cards.end(), f_move.card);
    f_cards.erase(card_pos);
}

int switch_player(int f_player_id, const std::vector<std::vector<int>> &f_cards)
{
    const auto cur_player = f_player_id;
    do
    {
        f_player_id = (f_player_id + 1) % static_cast<int>(f_cards.size());
    } while (f_cards[f_player_id].size() == 0 && cur_player != f_player_id);
    return f_player_id;
}

void draw_cards(std::vector<int> &f_src, std::vector<int> &f_dest, int f_num_cards)
{
    std::move(f_src.rbegin(), f_src.rbegin() + f_num_cards, std::back_inserter(f_dest));  // unsorted
    f_src.erase(std::prev(f_src.end(), f_num_cards), f_src.end());
}

std::vector<std::vector<int>> distribute_cards(std::vector<int> &      f_unplayed_cards,
                                               const std::vector<int> &f_hand_cards,
                                               const std::vector<int> &f_cards_per_player,
                                               int                     f_player_id)
{
    const int num_players = static_cast<int>(f_cards_per_player.size());
    auto      all_cards   = std::vector<std::vector<int>>(num_players, std::vector<int>{});

    all_cards[f_player_id] = f_hand_cards;
    for (int i = 0; i < num_players; ++i)
    {
        if (i != f_player_id)
        {
            if (f_cards_per_player[i] > static_cast<int>(f_unplayed_cards.size()))
            {
                throw std::runtime_error("mismatch of cards in game");
            }
            draw_cards(f_unplayed_cards, all_cards[i], f_cards_per_player[i]);
        }
    }

    return all_cards;
}

}  // namespace

GameState GameState::from_unknown(std::vector<int> &      f_unplayed_cards,
                                  const std::vector<int> &f_stacks,
                                  const std::vector<int> &f_hand_cards,
                                  const std::vector<int> &f_cards_per_player,
                                  int                     f_player_id)
{
    auto all_cards = distribute_cards(f_unplayed_cards, f_hand_cards, f_cards_per_player, f_player_id);
    return GameState(DeckViewer(f_unplayed_cards), f_stacks, all_cards, f_player_id);
}

void GameState::apply_turn(const std::vector<CardMove> &f_turn)
{
    for (const auto &move : f_turn)
    {
        do_move(m_stacks, m_cards[m_player_id], move);
    }
    m_deck.draw_cards(m_cards[m_player_id], static_cast<int>(f_turn.size()));
    m_player_id = switch_player(m_player_id, m_cards);
}

float GameState::rollout(int f_turn_length)
{
    while (true)
    {
        if (m_deck.empty())
        {
            f_turn_length = TURN_LENGTH_MIN;
        }
        constexpr int num_turns  = 1;
        auto          best_turns = thegame::generate_turns(m_stacks, m_cards[m_player_id], f_turn_length, num_turns);
        if (best_turns.empty())
        {
            break;
        }
        apply_turn(best_turns[0]);
    }

    return REWARD_WON - static_cast<float>(count_remaining_cards());
}

}  // namespace thegame