#ifndef THEGAME_MCTS_HPP
#define THEGAME_MCTS_HPP

#include "thegame/core.hpp"
#include "thegame/gamestate.hpp"

#include <memory>

namespace thegame
{
namespace mcts
{
class Tree
{
   public:
    class Node;

    Tree(const GameState& f_gamestate, int f_turn_length, int f_turn_width, float f_uct_c, bool f_multithreaded);
    // defined as default in cpp since unique_ptr requires it
    ~Tree();
    Tree(Tree&&);
    Tree& operator=(Tree&&);

    Tree(const Tree&) = delete;
    Tree& operator=(const Tree&) = delete;

    bool add_node(GameState& f_gamestate);

    std::vector<std::vector<CardMove>> get_turns() const;

    int size() const;

    int height() const;

    void _debug_print() const;

   private:
    std::unique_ptr<Node> m_root;
    int                   m_turn_length;
    int                   m_turn_width;
    float                 m_uct_c;
    bool                  m_multithreaded;
};

template <typename Selector>
inline int select_turn(const std::vector<float>& f_rewards,
                       const std::vector<float>& f_visits,
                       const Selector&           f_selector)
{
    int  best_idx   = 0;
    auto best_value = f_selector(f_rewards[0], f_visits[0]);
    for (int idx = 1; idx < static_cast<int>(f_rewards.size()); ++idx)
    {
        const auto value = f_selector(f_rewards[idx], f_visits[idx]);
        if (value > best_value)
        {
            best_value = value;
            best_idx   = idx;
        }
    }
    return best_idx;
}

int select_turn_uct(const std::vector<float>& f_rewards, const std::vector<float>& f_visits, float f_uct_c);

}  // namespace mcts
}  // namespace thegame

#endif