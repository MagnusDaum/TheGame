#ifndef THEGAME_GAMESTATE_HPP
#define THEGAME_GAMESTATE_HPP

#include "thegame/core.hpp"

#include <vector>

namespace thegame
{
constexpr float REWARD_WON = STACK_HIGH - STACK_LOW - 1;

class GameState
{
   public:
    GameState(DeckViewer f_deck, std::vector<int> f_stacks, std::vector<std::vector<int>> f_cards, int f_player_id)
        : m_deck(f_deck), m_stacks(f_stacks), m_cards(f_cards), m_player_id(f_player_id)
    {
    }

    // create game state from unknown deck and cards of other players by randomly assigning cards
    static GameState from_unknown(std::vector<int> &      f_unplayed_cards,
                                  const std::vector<int> &f_stacks,
                                  const std::vector<int> &f_hand_cards,
                                  const std::vector<int> &f_cards_per_player,
                                  int                     f_player_id);

    int count_remaining_cards() const
    {
        int num_cards = 0;
        for (const auto &cards : m_cards) num_cards += static_cast<int>(cards.size());
        return m_deck.size() + num_cards;
    }

    int get_player() const { return m_player_id; }

    bool deck_empty() const { return m_deck.empty(); }

    auto generate_turns(int f_turn_length, int f_num_turns) const
    {
        return thegame::generate_turns(m_stacks, m_cards[m_player_id], f_turn_length, f_num_turns);
    }

    void apply_turn(const std::vector<CardMove> &f_turn);

    float rollout(int f_turn_length);

   private:
    DeckViewer                    m_deck;
    std::vector<int>              m_stacks;
    std::vector<std::vector<int>> m_cards;
    int                           m_player_id;
};

}  // namespace thegame

#endif
