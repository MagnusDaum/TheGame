#ifndef THEGAME_CORE_HPP
#define THEGAME_CORE_HPP

#include <limits>
#include <tuple>
#include <vector>

namespace thegame
{
constexpr int JUMP_VALUE      = 10;
constexpr int STACK_HIGH      = 100;
constexpr int STACK_LOW       = 1;
constexpr int TURN_LENGTH_MIN = 1;

// use fixed-width integers to reduce memory consumption
using card_t = std::int_least8_t;
static_assert(std::numeric_limits<card_t>::max() > STACK_HIGH, "invalid integer width for representing cards");
using pymove_t = std::tuple<card_t, card_t>;

struct CardMove
{
    constexpr CardMove(int f_stack_id, int f_card)
        : stack_id(static_cast<card_t>(f_stack_id)), card(static_cast<card_t>(f_card))
    {
    }
    constexpr      operator pymove_t() const { return {stack_id, card}; }
    constexpr bool operator==(const CardMove &f_rhs) const
    {
        return (stack_id == f_rhs.stack_id) && (card == f_rhs.card);
    }

    card_t stack_id;
    card_t card;
};

class DeckViewer
{
   public:
    DeckViewer(const std::vector<int> &f_deck) : m_rbegin(f_deck.rbegin()), m_rend(f_deck.rend()) {}

    bool empty() const { return m_rbegin == m_rend; }

    int size() const { return static_cast<int>(std::distance(m_rbegin, m_rend)); }

    int draw_cards(std::vector<int> &f_dest, int f_num_cards)
    {
        const int num_cards_drawn = std::min(f_num_cards, size());
        std::copy(m_rbegin, m_rbegin + num_cards_drawn, std::back_inserter(f_dest));  // unsorted
        std::advance(m_rbegin, num_cards_drawn);
        return num_cards_drawn;
    }

   private:
    // since the deck card order is fixed, instead of using the vector and resizing it repeatedly, we only keep
    // begin/end iterators and move these around.
    std::vector<int>::const_reverse_iterator m_rbegin;
    std::vector<int>::const_reverse_iterator m_rend;
};

std::vector<std::vector<CardMove>> generate_turns(const std::vector<int> &f_stacks,
                                                  const std::vector<int> &f_cards,
                                                  int                     f_turn_length,
                                                  int                     f_num_turns);

}  // namespace thegame

#endif
