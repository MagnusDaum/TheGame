mkdir -p build
cd build
# for windows you might want to add something like: -G "Visual Studio 15 2017" -A x64
cmake -Dpybind11_DIR=`python -m pybind11 --cmakedir` -DCMAKE_BUILD_TYPE=Release ..
make
cd ..