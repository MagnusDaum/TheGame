#include "thegame/core.hpp"
#include "thegame/gamestate.hpp"
#include "thegame/mcts.hpp"

#include "omp.h"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>
#include <random>
#include <sstream>
#include <string>
#include <vector>

namespace py = pybind11;

namespace thegame
{
class BotMonteCarlo
{
   public:
    BotMonteCarlo(int   f_num_rollouts,
                  int   f_rollout_width,
                  int   f_rollout_turn_length,
                  float f_uct_c,
                  bool  f_multithreaded,
                  int   f_seed)
        : BotMonteCarlo(
              f_num_rollouts, f_rollout_width, f_rollout_turn_length, f_uct_c, f_multithreaded, std::mt19937(f_seed))
    {
    }

    BotMonteCarlo(int          f_num_rollouts,
                  int          f_rollout_width,
                  int          f_rollout_turn_length,
                  float        f_uct_c,
                  bool         f_multithreaded,
                  std::mt19937 rng)
        : m_rng_gen(rng),
          m_num_rollouts(f_num_rollouts),
          m_rollout_width(f_rollout_width),
          m_rollout_turn_length(f_rollout_turn_length > 0 ? f_rollout_turn_length : 1),
          m_uct_c(f_uct_c),
          m_multithreaded(f_multithreaded)
    {
    }

    void seed(int f_seed) { m_rng_gen.seed(f_seed); }

    std::vector<pymove_t> compute_turn(const std::vector<int> &f_deck,
                                       const std::vector<int> &f_stacks,
                                       const std::vector<int> &f_hand_cards,
                                       const std::vector<int> &f_cards_per_player,
                                       int                     f_player_id,
                                       int                     f_target_length)
    {
        int  best_turn_id = 0;
        auto all_turns    = generate_turns(f_stacks, f_hand_cards, f_target_length, m_rollout_width);
        if (all_turns.empty())
        {
            return {};
        }
        else if (all_turns.size() > 1)
        {
            // perform rollouts
            auto rewards = std::vector<float>(all_turns.size(), 0.0F);
            auto visits  = std::vector<float>(all_turns.size(), 0.0F);

            #pragma omp parallel for if (m_multithreaded)
            for (int roll_id = 0; roll_id < m_num_rollouts; ++roll_id)
            {
                int turn_id = roll_id < static_cast<int>(all_turns.size())
                                  ? roll_id
                                  : mcts::select_turn_uct(rewards, visits, m_uct_c);

                auto deck = std::vector<int>(f_deck);
                std::shuffle(deck.begin(), deck.end(), m_rng_gen);
                auto gamestate = GameState::from_unknown(deck, f_stacks, f_hand_cards, f_cards_per_player, f_player_id);

                gamestate.apply_turn(all_turns[turn_id]);

                const auto reward = gamestate.rollout(m_rollout_turn_length);
                #pragma omp critical
                {
                    rewards[turn_id] += reward;
                    ++visits[turn_id];
                }
            }

            // select best turn
            const auto selector = [](float r, float v) {
                return (v > 0.0F) ? (r / v) : std::numeric_limits<float>::lowest();
            };
            best_turn_id = mcts::select_turn(rewards, visits, selector);
        }

        // conversion to tuple for python
        auto result = std::vector<pymove_t>{};
        for (const auto &m : all_turns[best_turn_id])
        {
            result.push_back(m);
        }
        return result;
    }

    py::tuple to_py_tuple() const
    {
        std::stringbuf buffer{};
        std::ostream   os(&buffer);
        os << m_rng_gen;

        return py::make_tuple(
            m_num_rollouts, m_rollout_width, m_rollout_turn_length, m_uct_c, m_multithreaded, buffer.str());
    }

    static BotMonteCarlo from_py_tuple(const py::tuple &t)
    {
        if (t.size() != 6) throw std::runtime_error("invalid size of pickle state!");

        std::stringbuf buffer(t[t.size() - 1].cast<std::string>());
        std::istream   is(&buffer);
        std::mt19937   rng;
        is >> rng;

        return BotMonteCarlo(
            t[0].cast<int>(), t[1].cast<int>(), t[2].cast<int>(), t[3].cast<float>(), t[4].cast<bool>(), rng);
    }

   private:
    ///////////////////////////////////////////////////////////////////////////
    // member variables

    std::mt19937 m_rng_gen;
    int          m_num_rollouts;
    int          m_rollout_width;
    int          m_rollout_turn_length;
    float        m_uct_c;
    bool         m_multithreaded;
};

}  // namespace thegame

PYBIND11_MODULE(_bot_monte_carlo, m)  // add _ prefix to make distinguishable from python module
{
    m.doc() = "Bot Monte Carlo C++";

    py::class_<thegame::BotMonteCarlo>(m, "BotMonteCarlo")
        .def(py::init<int, int, int, float, bool, int>(),
             py::arg("num_rollouts")        = 100,
             py::arg("rollout_width")       = 0,
             py::arg("rollout_turn_length") = 1,
             py::arg("uct_c")               = thegame::STACK_HIGH - thegame::STACK_LOW,  // TODO
             py::arg("multithreaded")       = false,
             py::arg("random_seed")         = 42)
        .def("seed", &thegame::BotMonteCarlo::seed, py::arg("random_seed"))
        .def("compute_turn", &thegame::BotMonteCarlo::compute_turn)
        .def(py::pickle(
            [](const thegame::BotMonteCarlo &p) {  // __getstate__
                return p.to_py_tuple();
            },
            [](py::tuple t) {  // __setstate__
                return thegame::BotMonteCarlo::from_py_tuple(t);
            }));
}
